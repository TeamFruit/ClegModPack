val ccore = <ore:circuitUltimate>;
val ac = <ore:circuitBasic>;
val pc = <ore:circuitAdvanced>;
val ic = <ore:circuitElite>;

ccore.remove(<clayium:itemMisc:6>);
ac.remove(<clayium:itemMisc:3>);
pc.remove(<clayium:itemMisc:4>);
ic.remove(<clayium:itemMisc:5>);